package data;

import org.json.simple.JSONObject;
/* Questa è una classe "di servizio" che elabora i dati meteo */

public class DatiMeteo {

	private String data;
	private Number temperatura;
	private Number umidita;
	private Number vento;
	private Number pressione;
	
	/* Costruttore: data una stringa csv costruisce l'oggetto */
	public DatiMeteo (String csv) {
// to do
	}
	/* Costruttore: dato il JSON costruisce l'oggetto */
	public DatiMeteo (JSONObject j) {
// todo
	}
	/* Costruisce il JSON dato l'oggetto */
	@SuppressWarnings("unchecked")
	public JSONObject toJson() {
		JSONObject x = new JSONObject();
		// to do
		return x;
	}
	/* Convertitore printer friendly */
	public String toString() {
		String s = "to do";
		return s; 
	}	
	/* Selettori assortiti... */
	public String getData() {
		return data;
	}
	public Number getTemperatura() {
		return temperatura;
	}
	public Number getUmidita() {
		return umidita;
	}
	public Number getVento() {
		return vento;
	}
	public Number getPressione() {
		return pressione;
	}


}