package test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import data.DatiMeteo;

public class Test {
	public static void main(String[] args) throws IOException {
		BufferedReader input = new BufferedReader(new FileReader(args[0]));
		String l = "";
		while ( ( l = input.readLine() )  != null ) {
			DatiMeteo d = new DatiMeteo(l);
			System.out.println(d);
		}
		input.close();
	}
}
